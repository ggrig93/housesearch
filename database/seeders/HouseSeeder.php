<?php

namespace Database\Seeders;

use App\Models\House;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = [
            ['name'=>'Victoria','price'=>374662,'bedrooms'=>4,'bathrooms'=>2,'storeys'=>2,'garages'=>2,],
            ['name'=>'Xavier','price'=>513268,'bedrooms'=>4,'bathrooms'=>2,'storeys'=>1,'garages'=>2,],
            ['name'=>'Como','price'=>454990,'bedrooms'=>4,'bathrooms'=>3,'storeys'=>2,'garages'=>3,],
            ['name'=>'Aspen','price'=>384356,'bedrooms'=>4,'bathrooms'=>2,'storeys'=>2,'garages'=>2,],
            ['name'=>'Lucretia','price'=>572002,'bedrooms'=>4,'bathrooms'=>3,'storeys'=>2,'garages'=>2,],
            ['name'=>'Toorak','price'=>521951,'bedrooms'=>5,'bathrooms'=>2,'storeys'=>1,'garages'=>2,],
            ['name'=>'Skyscape','price'=>263604,'bedrooms'=>3,'bathrooms'=>2,'storeys'=>2,'garages'=>2,],
            ['name'=>'Clifton','price'=>386103,'bedrooms'=>3,'bathrooms'=>2,'storeys'=>1,'garages'=>1,],
            ['name'=>'Geneva','price'=>390600,'bedrooms'=>4,'bathrooms'=>3,'storeys'=>2,'garages'=>2,],

        ];

        foreach ($data as $item) {
            House::firstOrCreate($item);
        }
    }
}
