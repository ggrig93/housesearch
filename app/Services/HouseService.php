<?php
namespace App\Services;

use App\Interface\HouseServiceInterface;
use App\Models\House;

class HouseService implements HouseServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function getHouses(array $data): mixed
    {

        return House::query()
            ->when($data['searchName'], function ($query) use ($data){
                $query->where('name', 'LIKE', '%' . $data['searchName'] . '%');
                    })
            ->when($data['searchBathrooms'], function ($query) use ($data) {
                $query->where('bathrooms', '=', (int)$data['searchBathrooms']);
            })
            ->when($data['searchBedrooms'], function ($query) use ($data) {
                $query->where('bedrooms','=', (int)$data['searchBedrooms']);
            })
            ->when($data['searchStoreys'], function ($query) use ($data) {
                $query->where('storeys', (int)$data['searchStoreys']);
            })
            ->when($data['searchGarages'], function ($query) use ($data) {
                $query->where('garages', (int)$data['searchGarages']);
            })
            ->when($data['searchPrice']['min'], function ($query) use ($data) {
                $query->where('price', '>=', $data['searchPrice']['min']);
            })
            ->when($data['searchPrice']['max'], function ($query) use ($data) {
                $query->where('price', '<=', $data['searchPrice']['max']);
            })
       ->get();
    }
}
