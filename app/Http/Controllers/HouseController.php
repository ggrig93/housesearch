<?php

namespace App\Http\Controllers;

use App\Interface\HouseServiceInterface;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    /**
     * @var HouseServiceInterface
     */
    private HouseServiceInterface $houseService;
    public function __construct(HouseServiceInterface $houseService)
    {
        $this->houseService = $houseService;
    }

    public function getHouses(Request $request)
    {
        return $this->houseService->getHouses($request->all());
    }
}
