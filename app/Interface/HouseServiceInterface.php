<?php
namespace App\Interface;

interface HouseServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function getHouses(array $data): mixed;
}
