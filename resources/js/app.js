import { createApp } from 'vue'
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import App from './App.vue'
const app = createApp(App);

import router from './router/index.js'
app.use(ElementPlus);

// use router
app.use(router)
app.mount('#app')
